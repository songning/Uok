# Uok U盘快捷方式病毒专杀

#### 介绍
U盘快捷方式病毒专杀
针对U盘vbs快捷方式病毒，清除病毒文件和注册表被感染项目，恢复被隐藏的文件。
欢迎有兴趣的朋友一起改进！

#### 软件架构
本软件使用aardio编写。


#### 安装教程

下载“\Publish\Uok - U 盘快捷方式病毒杀毒修复工具 beta 1.0.exe"，直接打开即可使用。

#### 使用说明

注意：程序会自动查找敏感位置的.vbs文件，如果这些文件是你需要保留的，查杀时输入“Y”保留。

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)